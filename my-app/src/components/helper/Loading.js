import React from "react";
import styled from "styled-components";

const Background = styled.div`
  background: rgba(255, 255, 255, 0.6);
  min-height: 100vh;
  min-width: 100vw;
  height: 100%;
  width: 100%;
  position: absolute;
  z-index: 1;
`;

//Este loader foi retirado de: https://loading.io/css/

const Loader = styled.div`
  position: relative;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 10000;
  width: 8.25rem;
  height: 8.25rem;
  div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 8.25rem;
    height: 8.25rem;
    margin: 8px;
    border: 8px solid #57bbbc;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #57bbbc transparent transparent transparent;
  }
  div:nth-child(1) {
    animation-delay: -0.45s;
  }
  div:nth-child(2) {
    animation-delay: -0.3s;
  }
  div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

const Loading = () => {
  return (
    <Background>
      <Loader>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </Loader>
    </Background>
  );
};

export default Loading;
