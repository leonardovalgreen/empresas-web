import React from "react";
import styled from "styled-components";
import LogoIoasys from "../../static/img/logoHome.png";

const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 21rem;
`;

const TheLogo = styled.img`
  display: block;
  width: 18.438rem;
  height: 4.5rem;
  margin-bottom: 4rem;
`;

const Welcome = styled.h1`
  font-weight: bold;
  text-transform: uppercase;
  text-align: center;
  font-size: 1.6rem;
  letter-spacing: -1.2px;
  color: #383743;
  margin-bottom: 1.5rem;
`;

const WelcomeText = styled.span`
  text-align: center;
  font-size: 1.063rem;
  line-height: 1.569rem;
  letter-spacing: 0.2px;
  color: #383743;
`;

const Content = () => {
  return (
    <ContentContainer>
      <TheLogo src={LogoIoasys} alt="Logo ioasys" />
      <Welcome>
        bem-vindo ao <br></br> empresas
      </Welcome>
      <WelcomeText>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </WelcomeText>
    </ContentContainer>
  );
};

export default Content;
