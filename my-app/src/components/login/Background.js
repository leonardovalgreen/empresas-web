import React from "react";
import styled from "styled-components";

export const TheBackground = styled.div`
  height: 100vh;
  width: 100vw;
  max-height: 100%;
  max-width: 100%;
  background: #eeecdb;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Background = () => {
  return <div></div>;
};

export default Background;
