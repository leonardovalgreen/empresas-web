import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faExclamationCircle,
  faEye,
  faEyeSlash,
} from "@fortawesome/free-solid-svg-icons";

const Content = styled.div`
  position: relative;
`;

const TheInput = styled.input`
  position: relative;
  outline: none;
  font-size: 1.369rem;
  letter-spacing: -0.31px;
  padding-left: 2rem;
  color: #403e4d;
  border: none;
  border-bottom: 0.6px solid #383743;
  border-bottom-color: ${(props) =>
    props.validationFailed ? "#ff0f44" : "#383743"};
  background: unset;
  width: 21.75rem;
  border-radius: unset;
  &:nth-child(1) {
    margin: 1.5rem 0 1rem 0;
  }
  &:nth-child(2) {
    font-family: "Lucida Grande New", Arial, Helvetica, sans-serif;
  }
  &::-ms-reveal {
    display: none;
  }
  &::-ms-clear {
    display: none;
  }
`;

const Icon = styled.img`
  position: absolute;
  width: 1.625rem;
  left: 0;
  bottom: 1rem;
`;

const SideIcon = styled(FontAwesomeIcon)`
  position: absolute;
  width: 1.813rem;
  right: ${(props) => (props.fail === "false" ? "0px" : "20px")};
  bottom: 1.25rem;
  color: rgba(0, 0, 0, 0.54);
`;

const ErrorIcon = styled(SideIcon)`
  color: #ff0f44;
  right: 0;
`;

const Input = ({
  id,
  value,
  setValue,
  required,
  type,
  icon,
  password,
  validationFailed,
  fail,
  ...props
}) => {
  const [eyeSlash, setEyeSlash] = React.useState(false);
  const [theType, setTheType] = React.useState("password");
  function handleClick() {
    setEyeSlash(!eyeSlash);
    setTheType(theType === "text" ? "password" : "text");
  }
  return (
    <Content>
      <TheInput
        icon={icon}
        id={id}
        name={id}
        type={type === "password" ? theType : type}
        value={value}
        password={password}
        validationFailed={validationFailed}
        onChange={({ target }) => setValue(target.value)}
        required={required}
        {...props}
      />
      <Icon src={icon} />
      {password ? (
        <SideIcon
          fail={validationFailed.toString()}
          icon={eyeSlash ? faEye : faEyeSlash}
          onClick={handleClick}
        />
      ) : null}
      {validationFailed ? <ErrorIcon icon={faExclamationCircle} /> : null}
    </Content>
  );
};

export default Input;
