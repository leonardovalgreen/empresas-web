import React from "react";
import styled from "styled-components";
import Loading from "../helper/Loading";
import Input from "./Input";
import Lock from "../../static/img/ic-cadeado.svg";
import Email from "../../static/img/ic-email.svg";

const TheForm = styled.form`
  display: flex;
  flex-direction: column;
`;

const TheButton = styled.button`
  text-transform: uppercase;
  border: none;
  border-radius: 3.9px;
  width: 21.75rem;
  height: 3.563rem;
  margin: 2.563rem 0 0 0;
  background: #57bbbc;
  font-size: 1.208rem;
  font-weight: 600;
  color: #fff;
`;

const Form = () => {
  const [load, setLoad] = React.useState(false);
  const [login, setLogin] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [fail, setFail] = React.useState(false);

  async function handleLogin(event) {
    event.preventDefault();
    setLoad(true);
    console.log(login);
    try {
      let response = await fetch(
        "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: login,
            password: password,
          }),
        }
      );
      let responseJSON = await response.json();
      console.log(response, responseJSON);
      if (response.status !== 200) {
        setFail(true);
      }
    } catch {
      setFail(true);
    } finally {
      setLoad(false);
    }
  }

  return (
    <>
      {load ? <Loading /> : null}
      <TheForm onSubmit={handleLogin}>
        <Input
          id="Login"
          value={login}
          setValue={setLogin}
          required
          type="email"
          validationFailed={fail}
          icon={Email}
        />
        <Input
          id="Senha"
          value={password}
          setValue={setPassword}
          required
          type="password"
          validationFailed={fail}
          icon={Lock}
          password={true}
        />
        <TheButton>entrar</TheButton>
      </TheForm>
    </>
  );
};

export default Form;
