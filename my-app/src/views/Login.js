import React from "react";
import { TheBackground } from "../components/login/Background";
import Content from "../components/login/Content";
import Form from "../components/login/Form";

const Login = () => {
  return (
    <TheBackground>
      <Content />
      <Form />
    </TheBackground>
  );
};

export default Login;
